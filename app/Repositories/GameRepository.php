<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\Game;
use App\Models\Question;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class GameRepository
{
    /**
     * @return Game
     */
    public function create(): Game
    {
        $id = str_replace('-', '', Str::uuid()->toString());
        $game = new Game([]);
        $game->id = $id;
        $game->save();
        $game->id = $id;
        return $game;
    }

    /**
     * @param Game $game
     * @param Question $question
     */
    public function storeNexQuestion(Game $game, Question $question): void
    {
        DB::transaction(function () use ($question, $game) {
            $question->game = $game->id;
            $question->save();
            $game->currentQuestionId = $question->id;
            $game->save();
        });
    }

    /**
     * @param Game $game
     * @return Question
     */
    public function currentGameQuestion(Game $game): Question
    {
        return $game->currentQuestion()->firstOrFail();
    }

    /**
     * @param Game $game
     */
    public function update(Game $game): void
    {
        $game->save();
    }
}
