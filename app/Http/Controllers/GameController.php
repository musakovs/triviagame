<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Game;
use App\Services\GameService;
use Exception;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     * @var GameService
     */
    private $gameService;

    /**
     * GameController constructor.
     * @param GameService $gameService
     */
    public function __construct(GameService $gameService)
    {
        $this->gameService = $gameService;
    }

    public function newGame()
    {
        $game = $this->gameService->startGame();

        return redirect("/game/$game->id");
    }

    /**
     * @param Game $game
     * @return \Illuminate\Contracts\View\View|string
     */
    public function index(Game $game)
    {
        if ($game->finished()) {
            return 'Game finished: You ' . ($game->status ? 'win' : 'loose');
        }

        return view('game', ['pageData' => [
            'id' => $game->id
        ]]);
    }

    /**
     * @param Game $game
     * @return array
     * @throws Exception
     */
    public function nextQuestion(
        Game $game
    ): array
    {
        if ($game->finished()) {
            throw new Exception('game is finished');
        }

        $question = $this->gameService->getQuestion($game);

        return [
            'question' => $question->getQuestion(),
            'answers'  => $this->gameService->generateAnswers($question)
        ];
    }

    /**
     * @param Game $game
     * @param Request $request
     * @return int[]
     * @throws Exception
     */
    public function provideAnswer(Game $game, Request $request): array
    {
        if ($game->finished()) {
            throw new Exception('game finished');
        }

        $result = $this->gameService->provideAnswer($game, $request->post('answer'));

        return ['success' => $result];
    }
}
