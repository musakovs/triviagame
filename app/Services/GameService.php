<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Game;
use App\Models\Question;
use App\Repositories\GameRepository;
use App\Services\GameQuestionProviders\QuestionProviderInterface;

class GameService
{
    const STATUS_FAILED     = 0;
    const STATUS_IN_PROCESS = 1;
    const STATUS_WINN       = 2;

    private $answersToWinn = 20;

    /**
     * @var QuestionProviderInterface
     */
    private $questionProvider;

    /**
     * @var GameRepository
     */
    private $gameRepository;

    /**
     * GameService constructor.
     * @param QuestionProviderInterface $questionProvider
     * @param GameRepository $gameRepository
     */
    public function __construct(
        QuestionProviderInterface $questionProvider,
        GameRepository $gameRepository
    )
    {
        $this->questionProvider = $questionProvider;
        $this->gameRepository   = $gameRepository;
    }

    /**
     * @return Game
     */
    public function startGame(): Game
    {
        return $this->gameRepository->create();
    }

    /**
     * @param Game $game
     * @return Question
     */
    public function getQuestion(Game $game): Question
    {
        if ($game->currentQuestionId) {
            $question = $this->gameRepository->currentGameQuestion($game);
        } else {
            $question = $this->questionProvider->getQuestion($game);

            $this->gameRepository->storeNexQuestion($game, $question);
        }

        return $question;
    }

    /**
     * @param Game $game
     * @param $answer
     * @return int
     */
    public function provideAnswer(Game $game, $answer): int
    {
        $question = $this->gameRepository->currentGameQuestion($game);

        if ($answer == $question->answer) {
            $result = $this->processRightAnswer($game);
        } else {
            $result = $this->processWrongAnswer($game);
        }

        $this->gameRepository->update($game);

        return $result;
    }

    public function generateAnswers(Question $question): array
    {
        $result = [
            $question->getAnswer(),
            rand(1, 1000),
            rand(1, 1000),
            rand(1, 1000),
        ];

        shuffle($result);

        return $result;
    }

    /**
     * @param Game $game
     * @return int
     */
    private function processRightAnswer(Game $game): int
    {
        $game->increaseSuccess();
        if ($game->successCount >= $this->answersToWinn) {
            $game->finish(true);
            $result = self::STATUS_WINN;
        } else {
            $game->currentQuestionId = null;
            $result = self::STATUS_IN_PROCESS;
        }

        return $result;
    }

    /**
     * @param Game $game
     * @return int
     */
    private function processWrongAnswer(Game $game): int
    {
        $game->finish(false);
        return self::STATUS_FAILED;
    }
}
