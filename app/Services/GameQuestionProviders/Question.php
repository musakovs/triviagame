<?php

declare(strict_types=1);

namespace App\Services\GameQuestionProviders;

class Question
{
    /**
     * @var string
     */
    private $question;
    private $answer;

    public function __construct(string $question, $answer)
    {
        $this->question = $question;
        $this->answer = $answer;
    }

    /**
     * @return mixed
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @return string
     */
    public function getQuestion(): string
    {
        return $this->question;
    }
}
