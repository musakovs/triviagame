<?php

declare(strict_types=1);

namespace App\Services\GameQuestionProviders;

use App\Models\Game;
use App\Models\Question;
use GuzzleHttp\Client;

class NumbersQuestionProvider implements QuestionProviderInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * NumbersQuestionProvider constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param Game $game
     * @return Question
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getQuestion(Game $game): Question
    {
        $answers = $game->questions()->pluck('answer')->toArray();

        $answer = $this->generateAnswer($answers);

        $result = $this->client->get('http://numbersapi.com/' . $answer);

        $question = $result->getBody()->getContents();

        $question = trim(str_replace($answer, '', $question));

        return new Question([
            'question' => $question,
            'answer'   => $answer
        ]);
    }

    private function generateAnswer(array $exclude): int
    {
        $answer = rand(1, 1000);

        if (!in_array($answer, $exclude)) {
            return $answer;
        }

        return $this->generateAnswer($exclude);
    }
}
