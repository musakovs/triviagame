<?php

declare(strict_types=1);

namespace App\Services\GameQuestionProviders;

use App\Models\Game;

interface QuestionProviderInterface
{
    public function getQuestion(Game $game): \App\Models\Question;
}
