<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Question
 * @package App\Models
 *
 * @property int $id
 * @property string $game
 * @property string $question
 * @property $answer
 */
class Question extends Model
{
    protected $table = 'questions';
    protected $fillable = ['question', 'answer'];

    /**
     * @return string
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    public function getAnswer()
    {
        return $this->answer;
    }
}
