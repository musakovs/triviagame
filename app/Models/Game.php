<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

/**
 * Class Game
 * @package App\Models
 *
 * @property $id sting
 * @property $successCount int
 * @property $currentQuestionId int
 * @property $status int
 * @property $created_at
 * @property $updated_at
 */
class Game extends Model
{
    protected $table = 'games';
    protected $keyType = 'string';
    protected $primaryKey = 'id';

    public function currentQuestion(): HasOne
    {
        return $this->hasOne(Question::class, 'id', 'currentQuestionId');
    }

    public static function bootUuids()
    {
        static::creating(function (Model $model) {
            $model->{$model->getKeyName()} = Str::orderedUuid()->toString();
        });
    }

    public function finish(bool $success): void
    {
        $this->status = $success ? 2 : 0;
        $this->save();
    }

    /**
     * @return bool
     */
    public function finished(): bool
    {
        return $this->status !== 1;
    }

    /**
     * @return HasMany
     */
    public function questions(): HasMany
    {
        return $this->hasMany(Question::class, 'game', 'id');
    }

    public function increaseSuccess(): void
    {
        $this->successCount++;
    }
}
