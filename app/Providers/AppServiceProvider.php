<?php

namespace App\Providers;

use App\Models\Game;
use App\Services\GameQuestionProviders\NumbersQuestionProvider;
use App\Services\GameQuestionProviders\QuestionProviderInterface;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(QuestionProviderInterface::class, NumbersQuestionProvider::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //todo this does not works in Laravel 8
//        Game::creating(function (Game $game) {
//            $game->id = str_replace('-', '', Str::uuid()->toString());
//        });
    }
}
