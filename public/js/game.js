$(function() {

    const nextQuestion = (process = true) => {
        if (process) {
            return $.ajax({
                url: `/game/${pageData.id}/next-question`,
                method: 'get'
            }).then((response) => {
                const {question, answers} = response;
                $('#question').html(question);
                generateAnswers(answers);
            })
        }
    }

    const generateAnswers = (answers) => {
        let html = '';

        answers.forEach((answer) => {
            html += `<input type="radio" name="answer" value="${answer}">${answer}<br>`;
        })

        $('#answers').html(html);
    };

    const sendAnswer = (form) => {
        return $.ajax({
            url: `/game/${pageData.id}/answer`,
            method: 'post',
            data: $(form).serialize()
        });
    }

    const checkResponse = (response) => {
        if (response.success === 1) {
            alert('right, next question')
            return true;
        } else if (response.success === 2) {
            alert('Winn!');
            window.location.reload();
            return false;
        } else {
            alert('fail');
            window.location.reload();
            throw Error('');
        }
    }

    nextQuestion();

    $('#question-form').on('submit', function (e) {
        e.preventDefault();

        sendAnswer(this)
            .then(checkResponse)
            .then(nextQuestion)

        return true;
    });

})
