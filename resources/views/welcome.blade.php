@extends('layout')

@section('content')

    <h1 class="text-center">Hello to Trivia Game</h1>
    <div class="text-center">
        <a class="btn btn-lg btn-primary" href="game/start">Start</a>
    </div>

@endsection
