@extends('layout')

@section('scripts')
    <script src="/js/game.js"></script>
@endsection

@section('content')
    <div class="col-md-4 offset-4 mt-5 p-3 card">
        <form id="question-form">
        <div class="form-group">
            <label>Question:</label>
            <div id="question"></div>
        </div>
        <div class="form-group">
            <label for="answer">Your answer:</label>
            <div id="answers"></div>
        </div>
            <input type="text" name="id" value="{{$pageData['id']}}" hidden>
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
