<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

$gameController = 'App\\Http\\Controllers\\GameController';

Route::get('/game/start', "$gameController@newGame");
Route::get('/game/{game}', "$gameController@index");
Route::get('/game/{game}/next-question', "$gameController@nextQuestion");
Route::post('/game/{game}/answer', "$gameController@provideAnswer");


